# Trabajo Práctico: DockerFile

Este repositorio contiene los archivos y las instrucciones necesarias para implementar una página web estática utilizando Apache dentro de un contenedor Docker y configurar un servidor proxy reverso con NGINX. Además, se detalla el proceso para integrar el proyecto en un repositorio GitLab.

## Requisitos

- VirtualBox.
- Imagen de Ubuntu en formato OVA (preferiblemente versión 22.2 o superior).
- Cuenta de GitLab.
- Imagen de Apache para Docker.
- Binarios de NGINX.
- Binarios de Git.

## Desarrollo

### Preparación de VMs en VirtualBox

1. Descargar la imagen OVA para las máquinas virtuales desde el enlace proporcionado.
2. Importar la imagen OVA en VirtualBox.
3. Configurar las VMs con red NAT y asignar IPs específicas.
4. Actualizar la configuración de red de las VMs utilizando Netplan.

### Instalación de Software

1. Instalar Docker en la VM Docker.
2. Instalar NGINX en la VM NGINX.
3. Actualizar el sistema en ambas VMs.

### Implementación del Sitio Web

1. Preparar la estructura de archivos del proyecto.
2. Crear el contenido para el sitio web estático.
3. Crear un Dockerfile para Apache.
4. Construir la imagen de Docker para Apache.
5. Ejecutar el contenedor Docker para servir el sitio web.

### Pruebas y Gestión de Código

1. Configurar los puertos en VirtualBox para acceder al servidor web desde la máquina host.
2. Crear un repositorio en GitLab.
3. Clonar el repositorio en las VMs.
4. Copiar los archivos relevantes al repositorio.

## Reflexión

Este trabajo práctico proporciona una guía detallada y paso a paso para configurar un entorno de desarrollo que integra Docker con Apache para servir una página web estática y NGINX como proxy reverso. Además, se destaca la importancia de la gestión de código mediante GitLab, asegurando que el proyecto esté versionado y accesible para futuros desarrollos o revisiones. Esta metodología no solo facilita el despliegue de aplicaciones web, sino que también fomenta buenas prácticas en la gestión de infraestructuras y el uso de tecnologías de contenedorización.
